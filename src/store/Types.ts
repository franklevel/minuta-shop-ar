import { PaymentMethodName } from './checkout'


export interface PaymentMethodInformation {
  name: string;
  value: PaymentMethodName;
  icon: string;
  component: React.FunctionComponent;
}

export interface Field {
    name: string,
        required: boolean,
        type: string
}

export interface Metadata {
    id: string,
        name: string,
        instructions: string,
        fields: Field[]

}

export interface PaymentMethod {
    id: number,
        name: string,
        icon: string,
        value: PaymentMethodName,
        currency: string[],
        metadata: Metadata
}
