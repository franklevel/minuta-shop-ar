import { useState } from 'react'
import { useAtom } from 'jotai'
import { PaymentMethod } from '../store/Types'
import { PaymentMethodName, paymentGatewayAtom } from '../store/checkout'
const MOCK_DATA: PaymentMethod[] = [
  {
    "id": 1,
    "name": "Zelle",
      "value": 'ZELLE',
      "icon":"",
    "currency": [
      "USD"
    ],
    "metadata": {
      "id": "#",
      "name": "La gota fria",
      "fields": [
        {
          "name": "email",
          "required": true,
          "type": "email"
        }
      ],
      "instructions": "Los detalles de la operacion son los siguientes XXXX"
    }
  },
  {
    "id": 1,
    "name": "Reserve",
      "value":'RESERVE',
      "icon":"",
    "currency": [
      "USD",
      "VES"
    ],
    "metadata": {
      "id": "#",
      "name": "La gota fria",
      "fields": [
        {
          "name": "username",
          "required": true,
          "type": "text"
        }
      ],
      "instructions": "Transfiera al siguiente usuario XXXX y notifique el pago en YYYYYY"
    }
  },
  {
    "id": 1,
    "name": "Pago Movil",
      "value":"PAGO_MOVIL",
      "icon":"",
    "currency": [
      "VES"
    ],
    "metadata": {
      "id": "#",
      "name": "La gota fria",
      "fields": [
        {
          "name": "Pago Movil #",
          "required": true,
          "type": "text"
        }
      ],
      "instructions": "Transfiera al siguiente usuario XXXX y notifique el pago en YYYYYY"
    }
  }
]

const generatePaymentMethodsMap = (paymentMethods: PaymentMethod[]):Record<PaymentMethodName,PaymentMethod> =>{
    const paymentMap = {} as Record<PaymentMethodName,PaymentMethod>
    for (let method of paymentMethods){
        paymentMap[method.value] = method
    }
    return paymentMap
}

export const usePayment = () => {
  const [gateway, setGateway] = useAtom<PaymentMethodName>(paymentGatewayAtom);
  const [errorMessage, setErrorMessage] = useState<string | null>(null);
    const AVAILABLE_PAYMENT_METHODS_MAP: Record<PaymentMethodName,PaymentMethod> = generatePaymentMethodsMap(MOCK_DATA)
    const paymentMethod: PaymentMethod = AVAILABLE_PAYMENT_METHODS_MAP[gateway]
    return {
        AVAILABLE_PAYMENT_METHODS_MAP: generatePaymentMethodsMap(MOCK_DATA),
        gateway,
        setGateway,
        errorMessage,
        setErrorMessage,
        paymentMethod
    }
}
