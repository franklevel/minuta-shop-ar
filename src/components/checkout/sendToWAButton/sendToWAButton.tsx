import React from 'react'
import Button from '@components/ui/button'
import { useCart } from '@store/quick-cart/cart.context'
import { generateMessage } from '../utils/generateMessage'
import { usePayment } from '../../../hooks/usePayment'

// la app deberia crear la orden en el backend con los datos del carrito y luego obtener el numero de orden para enviarlo WA

const SendToWAButton = () => {

    const { paymentMethod } = usePayment()
    const { items, resetCart } = useCart()
    const handleClick = async () => {
        const ORDER_CODE = 'qwe456'
        const PHONE_NUMBER = ''
        const WA_URL = `https://api.whatsapp.com/send/?phone=%2B${PHONE_NUMBER}&text=${encodeURIComponent(generateMessage(items, paymentMethod, ORDER_CODE))}&app_absent=0`
        resetCart()
        window.location.href = WA_URL
    }
    return (
        <Button className='w-full mt-5' onClick={handleClick} >Send to WhatsApp</Button>
    )
}

export default SendToWAButton
