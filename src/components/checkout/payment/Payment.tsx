import React from 'react'
import { PaymentMethod } from '../../../store/Types'
import { Form } from '../../ui/forms/form'
import  Input  from '../../ui/forms/input'

const Payment = ({ paymentMethod }:{ paymentMethod: PaymentMethod }) => {
    return (
        <Form>
        {() => (
            <>
            <h2>{paymentMethod?.name}</h2>
            {paymentMethod?.metadata.fields.map(field => {
                const { name, type, required } =  field
               return (
                <Input name={name} type={type} required={required} key={name} label={name} />
            )})}
            <p>{paymentMethod?.metadata.instructions}</p>
            </>
        )}
        </Form>
    )
}

export default Payment
