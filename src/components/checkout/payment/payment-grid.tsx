import { RadioGroup } from '@headlessui/react';
import { useTranslation } from 'next-i18next';
import Alert from '@components/ui/alert';
import cn from 'classnames';
import Payment from './Payment';
import { usePayment } from '../../../hooks/usePayment'


// Payment Methods Mapping Object


const PaymentGrid: React.FC<{ className?: string }> = ({ className }) => {
    const { t } = useTranslation('common');
    const { AVAILABLE_PAYMENT_METHODS_MAP, errorMessage, setErrorMessage, gateway, setGateway, paymentMethod } = usePayment()
  return (
    <div className={className}>
      {errorMessage ? (
        <Alert
          message={t(`common:${errorMessage}`)}
          variant="error"
          closeable={true}
          className="mt-5"
          onClose={() => setErrorMessage(null)}
        />
      ) : null}

      <RadioGroup value={gateway} onChange={setGateway}>
        <RadioGroup.Label className="text-base text-heading font-semibold mb-5 block">
          {t('text-choose-payment')}
        </RadioGroup.Label>

        <div className="grid gap-4 grid-cols-2 md:grid-cols-3 mb-8">
          {Object.values(AVAILABLE_PAYMENT_METHODS_MAP).map(
            ({ name, icon, value }) => (
              <RadioGroup.Option value={value} key={value}>
                {({ checked }) => (
                  <div
                    className={cn(
                      'w-full h-full py-3 flex items-center justify-center border text-center rounded cursor-pointer relative',
                      checked
                        ? 'bg-light border-accent shadow-600'
                        : 'bg-light border-gray-200'
                    )}
                  >
                    {icon ? (
                      <>
                        {/* eslint-disable */}
                        <img src={icon} alt={name} className="h-[30px]" />
                      </>
                    ) : (
                      <span className="text-xs text-heading font-semibold">
                        {name}
                      </span>
                    )}
                  </div>
                )}
              </RadioGroup.Option>
            )
          )}
        </div>
      </RadioGroup>
      <div>
        <Payment paymentMethod={paymentMethod} />
      </div>
    </div>
  );
};

export default PaymentGrid;
