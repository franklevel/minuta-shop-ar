import { Item } from '@store/quick-cart/cart.utils'
import { PaymentMethod } from '@store/Types'

const generateProductList = (items: Item[]) => {

    return items.map(item => `_- ${item.quantity} ${item.name} $${item.price}_`).join('\n')
}
export const generateMessage = (items: Item[], paymentMethod: PaymentMethod, orderCode: string) => {
    return `
*Pedido:* _${orderCode}_
    
*Resumen:*
${generateProductList(items)}

*Metodo de Pago:*
_${paymentMethod.name}_

*Entrega*
{delivery_method} {delivery_address}

Puedes seguir el estado de tu envío en el siguiente enlace:
{domain}/tracking/${orderCode}`
}
